package com.moneydance.modules.features.hello;

import java.util.ArrayList;
import java.util.Arrays;

import com.hleofxquotes.md.MxtUtils;
import com.moneydance.apps.md.controller.Main;

public class HelloExtMain extends com.moneydance.apps.md.controller.Main {
    public static void main(String[] args) {
        boolean portable = true;

        if (portable) {
            MxtUtils.enablePortableMain(args);
        }

        Main.DEBUG = true;
        ArrayList<String> argList = new ArrayList<String>(Arrays.asList(args));
        argList.add("-d");
        args = argList.toArray(args);

        com.moneydance.apps.md.controller.Main.main(args);
    }
}
