/**
 * Copyright 2020 hleofxquotes@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.moneydance.modules.features.hello;

import java.awt.Component;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.hleofxquotes.md.AbstractFeatureTask;
import com.hleofxquotes.md.DefaultMxtFeatureModule;
import com.hleofxquotes.md.SampleHomePageView;
import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.hleofxquotes.md.task.MenuCommand;
import com.hleofxquotes.md.task.MenuCommandUi;
import com.hleofxquotes.md.task.RequestHandler;
import com.moneydance.apps.md.view.HomePageView;

/**
 * An example of Moneydance extension.
 */
public class Main extends DefaultMxtFeatureModule {

    /** The Constant LOGGER. */
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(Main.class.getName());

    /**
     * Instantiates a new main.
     */
    public Main() {
        super();

        LOGGER.info("> Main - " + getExtensionName() + ", classLoader=" + this.getClass().getClassLoader());
    }

    @Override
    public void init() {
        super.init();

        File sourceFile = this.getSourceFile();
        if (sourceFile != null) {
            LOGGER.info("  sourceFile=" + sourceFile.getAbsolutePath());
        }
    }

    /**
     * Inits the features.
     */
    @Override
    protected void initFeatures() {
        AbstractFeatureTask task = null;

        task = new MenuCommandUi("showdialog", "Show diaglog") {
            @Override
            public void run() {
                Component parentComponent = getMainFrame();
                JOptionPane.showMessageDialog(parentComponent, "Eggs are not supposed to be green.");
            }
        };
        addFeatureTask(task);

        task = new MenuCommand("sleep", "Sleep (bg)") {
            @Override
            public void run() {
                long millis = 5 * 1000L;
                try {
                    setConsoleStatus("> START Sleeping for millis=" + millis);
                    Thread.sleep(millis);
                } catch (InterruptedException e) {
                    LOGGER.warning(e.getMessage());
                } finally {
                    setConsoleStatus("< DONE Sleeping for millis=" + millis);
                }
            }
        };
        addFeatureTask(task);

        // if you need to access params, we need to do it in threadsafe task
        final String sayTimeCommand = "saytime";
        task = new RequestHandler(sayTimeCommand) {
            @Override
            public void run() {
                LOGGER.info("> THREAD_SAFE run()");
                final List<NameValuePair> tsParam = getParams();

                // once you've captured the params values, we can get off this threadsafe
                // thread.
                Runnable cmd = new Runnable() {
                    @Override
                    public void run() {
                        for (NameValuePair param : tsParam) {
                            LOGGER.info("Task=" + getName() + ": param.name=" + param.getName() + ", param.value="
                                    + param.getValue());
                        }
                        setConsoleStatus("Current time is: " + new Date());
                    }
                };
                getThreadPool().execute(cmd);
            }
        };
        addFeatureTask(task);

        task = new MenuCommand("asktime", "Ask time") {
            @Override
            public void run() {
                // eventName is a just a label
                String eventName = getName();

                // send to myself
                String toModule = getExtensionName();

                // ask for time
                String command = sayTimeCommand;

                BasicNameValuePair param = new BasicNameValuePair("name", "hleofxquotes");
                sendRequest(eventName, toModule, command, param);
            }
        };
        addFeatureTask(task);
    }

    /**
     * Inits the home page views.
     */
    @Override
    protected void initHomePageViews() {
        HomePageView homePageView = new SampleHomePageView(getMoneydanceGUI(), this, getContext());
//            homePageView.setActive(true);
        getHomePageViews().add(homePageView);
    }

}
