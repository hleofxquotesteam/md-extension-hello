package com.moneydance.apps.md.controller;

import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.moneydance.modules.features.hello.FeatureModuleTest;

public class ModuleLoaderTest {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(ModuleLoaderTest.class.getName());

    @Test
    public void testModuleLoader() {
        com.moneydance.apps.md.controller.Main main = new com.moneydance.apps.md.controller.Main();
        ModuleLoader moduleLoader = new ModuleLoader(main);
        File moduleFile = new File(FeatureModuleTest.TEST_MODULE_FILE_NAME);
        try {
            FeatureModule featureModule = moduleLoader.loadFeatureModule(FeatureModuleTest.MODULE_NAME, moduleFile, false);
            Assert.assertNotNull(featureModule);
            ClassLoader classLoader = featureModule.getClass().getClassLoader();
            LOGGER.info("classLoader=" + classLoader.getClass());

            File sourceFile = featureModule.getSourceFile();
            LOGGER.info("sourceFile=" + sourceFile);

            LOGGER.info("isVerified=" + featureModule.isVerified());

        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }

    private File getModuleFile(FeatureModule featureModule) throws NoSuchFieldException, IllegalAccessException {
        Field privateStringField = FeatureModule.class.getDeclaredField("moduleFile");
        privateStringField.setAccessible(true);
        File file = (File) privateStringField.get(featureModule);
        LOGGER.info("foundModuleFile=" + file);
        return file;
    }
}
