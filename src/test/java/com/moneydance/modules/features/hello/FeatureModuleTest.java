package com.moneydance.modules.features.hello;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.Test;

import com.hleofxquotes.md.MockFeatureModuleContext;
import com.hleofxquotes.md.MxtResourceLoader;
import com.hleofxquotes.md.logging.MxtSimpleFormatter;
import com.moneydance.apps.md.controller.FeatureModule;

public class FeatureModuleTest {
    private final static Logger LOGGER = MxtSimpleFormatter.getLogger(FeatureModuleTest.class.getName());
    public static final String MODULE_NAME = "hello";
    public static final String TEST_MODULE_FILE_NAME = "target/dist/" + MODULE_NAME + ".mxt";

    @Test
    public void testModule() throws MalformedURLException {
        FeatureModule module = new Main() {
            @Override
            public void init() {
                try {
                    MockFeatureModuleContext.setPrivateContext(this);
                } catch (Exception e) {
                    LOGGER.severe(e.getMessage());
                }

                super.init();
            }
        };

        Assert.assertNotNull(module.getName());

        Assert.assertEquals(MODULE_NAME, module.getName());

        File sourceFile = new File(TEST_MODULE_FILE_NAME);
        MxtResourceLoader mxtResourceLoader = new MxtResourceLoader(sourceFile);
        URL resource = mxtResourceLoader.getResource("com/moneydance/modules/features/" + MODULE_NAME + "/meta_info.dict");
        Assert.assertNotNull(resource);
        LOGGER.info("resource=" + resource.toExternalForm());

        module.init();

//        Assert.assertEquals(1, module.getBuild());

        module.cleanup();
        module.unload();
    }

}
